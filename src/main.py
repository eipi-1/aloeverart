# http: // www.pyqtgraph.org/


# -*- coding: utf-8 -*-
"""
Example demonstrating a variety of scatter plot features.
"""


## Add path to library (just for examples; you do not need this)
# import initExample
from plantstream import PlantSensor, RandomSensor
import math
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
import numpy as np
from collections import namedtuple

pg.setConfigOptions(antialias=True)


class PointGenerator():

    SAMPLE_SIZE = 50
    INITIAL_RADIUS = 10
    ANGLE = math.pi * 2 / SAMPLE_SIZE
    points = [(0, 0)] * SAMPLE_SIZE
    position = 0

    def __init__(self, sensor):
        self.sensor = sensor
        self.points = [(self.INITIAL_RADIUS * math.cos(i * self.ANGLE),
                        self.INITIAL_RADIUS * math.sin(i * self.ANGLE)) for i in range(self.SAMPLE_SIZE)]

    def get_next_point(self):
        self.position = (self.position + 1) % self.SAMPLE_SIZE
        plant_value = self.sensor.value
        # print(plant_value)

        # scale values to 100
        next_value = plant_value * 100

        # get direction of previous point
        old_point = self.points[self.position]
        old_x = old_point[0]
        old_y = old_point[1]
        old_magnitude = math.sqrt(old_x**2 + old_y**2)
        if old_magnitude > 0:
            old_x = old_x / old_magnitude
            old_y = old_y / old_magnitude
        
        # new point in old direction with new radius
        new_x = old_x * next_value
        new_y = old_y * next_value

        # add new length on top of old length
        new_point = (old_point[0] + new_x, old_point[1] + new_y)

        self.points[self.position] = new_point

        return new_point

    def create_generator(self):
        while True:
            yield self.get_next_point()




app = QtGui.QApplication([])
mw = QtGui.QMainWindow()
mw.resize(800, 800)
# GraphicsView with GraphicsLayout inserted by default
view = pg.GraphicsLayoutWidget()
mw.setCentralWidget(view)
mw.show()
mw.setWindowTitle('pyqtgraph example: ScatterPlot')

## create four areas to add plots
w1 = view.addPlot()

## There are a few different ways we can draw scatter plots; each is optimized for different types of data:


## 1) All spots identical and transform-invariant (top-left plot).
## In this case we can get a huge performance boost by pre-rendering the spot
## image and just drawing that image repeatedly.

s1 = pg.PlotCurveItem(size=10)# , pen=pg.mkPen(
    # None), brush=pg.mkBrush(255, 255, 255, 120))
X_POSITIONS = []
Y_POSITIONS = []

s1.setData(x=X_POSITIONS, y=Y_POSITIONS)
w1.addItem(s1)

## Make all plots clickable
# lastClicked = []


# def clicked(plot, points):
#     global lastClicked
#     for p in lastClicked:
#         p.resetPen()
#     print("clicked points", points)
#     for p in points:
#         p.setPen('b', width=2)
#     lastClicked = points


# s1.sigClicked.connect(clicked)

## Update every 50 ms


plant_sensor = PlantSensor(normalize=True)
plant_sensor.launch()
random_sensor = RandomSensor()

point_generator = PointGenerator(sensor=random_sensor).create_generator()
def update():
    new_point = next(point_generator)
    global X_POSITIONS, Y_POSITIONS
    X_POSITIONS.append(new_point[0])
    Y_POSITIONS.append(new_point[1])
    s1.setData(x=X_POSITIONS, y=Y_POSITIONS)
    # pw.plot(x, y, clear=True)


timer = pg.QtCore.QTimer()
timer.timeout.connect(update)
timer.start(10)

## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
