import threading
import time
import random
import serial


class PlantSensor:

    def __init__(self, port="/dev/ttyUSB0", baud=9600, normalize=True):
        self.port = port
        self.baud = baud
        self.value = 0
        self.stop_thread = False
        self.thread = None
        self.normalize = normalize


    def launch(self):
        if not self.thread:
            self.thread = threading.Thread(target=self.read_serial)
        else:
            self.stop()
        self.thread.start()


    def stop(self):
        self.stop_thread = True


    def read_serial(self):
        self.stop_thread = False
        serial_input = None
        with serial.Serial(self.port, self.baud, timeout=1) as serial_connection:
            while not self.stop_thread:
                try:
                    serial_input = serial_connection.readline().strip()
                    if self.normalize:
                        self.value = float(serial_input)/1023
                    else:
                        self.value = float(serial_input)
                except BaseException:
                    if serial_input:
                        print("Value is no float: {}".format(serial_input))
                    else:
                        print("Failed to read from serial")
                    time.sleep(.1)


class RandomSensor:

    @property
    def value(self):
        return random.random()
